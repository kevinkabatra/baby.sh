#!bin/bash

SetE () {
	echo $(date -ud "08/22/07" +"%B %d, %Y") $lastK".Count() ++"
	E=1
}

SetG () {
	echo $(date -ud "05/13/86" +"%B %d, %Y") $lastG".Count() ++"
	G=1
}

SetF () {
	echo $(date -ud "10/22/16" +"%B %d, %Y") "Family.Count() ++"
	F=$(($K+$G+$E+$L))
}

SetK () {
	echo $(date -ud "12/24/85" +"%B %d, %Y") $lastK".Count() ++"
	K=1
}

SetL () {
	echo $(date -ud "11/22/06" +"%B %d, %Y") $lastG".Count() ++"
	L=1
}

SetNew () {
	echo $(date -ud "03/26/18" +"%B %d, %Y")
	F=$(($F+1))
}

Populate () {
	SetK; echo "Number of" $lastK"'s =" $K; echo
 	SetG; echo "Number of" $lastG"'s =" $G; echo
	SetL; echo "Number of" $lastG"'s =" $(($G+$L)); echo
	SetE; echo "Number of" $lastK"'s =" $(($K+$E)); echo
	SetF; echo "Kevin and Gabby get married. Family size is now" $F "."; echo
	SetNew;
}

lastK='Kabatra'
lastG='Guay'

end=$(date -ud "2018-03-26 00:00:00")

while [ $(date +'%s') -lt $(date -ud "$end" +'%s') ]
do
	remainingSeconds=$(($(date -ud "$end" +'%s') - $(date +'%s') ))
	days=$(( $remainingSeconds/60/60/24 ))
	remainingSeconds=$(( $remainingSeconds - $(($days*60*60*24)) ))

	hours=$(( $remainingSeconds/60/60 ))
   	remainingSeconds=$(( $remainingSeconds - $(($hours*60*60)) ))

	minutes=$(( $remainingSeconds/60 ))
	remainingSeconds=$(( $remainingSeconds - $(($minutes*60)) ))

	clear
	Populate
	echo $days "day(s)," $hours "hour(s)," $minutes "minute(s)," $remainingSeconds "second(s) remaining."
    
	sleep 1
done